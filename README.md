What’s the idea?
- This pilot relies on current/historical dataset and builds a pilot around how to use the dataset with community 
- Interacts with EJScreen tool
- The pilot aims to provide a narrative of demonstrating how qualitative information around quantitative data can be used. “This thing happened in 2018 / we started doing monitoring / these three decisions were made / this is how it effected 3-5 EJ principles
- Future proof of concept (phase 2): how can usability of historic dataset in conjunction with community qualitative information provide a role in check and balance of environmental rules in action

